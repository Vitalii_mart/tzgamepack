﻿

using UnityEngine;

[CreateAssetMenu(fileName = "BallSettings", menuName = "RollerBall/BallSettings", order = 1)]
public class BallSettings : ScriptableObject {

	[Range(1f,5f)]
	public float DefaultSize = 3f;
	[Range(45f, 120f)]
	public float TurnSpeed = 90f;
	[Range(3f, 20f)]
	public float Asseleration = 5f;
	[Range(6f, 12f)]
	public float SpeedLimit = 8f;
	[Range(0.0001f, 0.1f)] [Tooltip("0.01 - за 100 очков + 1 метр")]
	public float ScoreToSizeMultiplier = 0.1f;

}
