﻿

using UnityEngine;
using System.Collections;

/// База объектов липучек
[RequireComponent(typeof(Collider))]
public class StickedObject : MonoBehaviour, IStickable, IScorable {


	[SerializeField][Range(1f, 10f)]
					 private float _lifetime = 2f;
	[SerializeField] protected HideType _hideType;
	[SerializeField] protected float _bonusScoreAmount;
	protected Collider _thisColl;
	protected IVelcro _stickedTo;

	protected virtual void Awake() {
		_thisColl = _thisColl ?? (_thisColl = GetComponent<Collider>());
	}

	private void OnTriggerEnter(Collider other) {
		other.GetComponent<IVelcro>()?.GlueMe(other.ClosestPoint(transform.position), this);
	}

	public float Lifetime => _lifetime;
	public float GetBonusScore() => _bonusScoreAmount;
	public virtual GameObject RootObject => gameObject;

	public virtual void OnGlued(IVelcro velcro) {
		_thisColl.enabled = false;
		_stickedTo = velcro;
	}
	public virtual void OnPeelOff() {

		switch (_hideType) {
		case HideType.None:
		case HideType.Peel:
			CoroutineBehavior.StartCoroutine(Peel(), () => Destroy(gameObject));
			break;
		case HideType.Drow:
			CoroutineBehavior.StartCoroutine(Drow(), () => Destroy(gameObject));
			break;
		}

		IEnumerator Peel() {
			gameObject.layer = LayerMask.NameToLayer("Ragdoll");
			transform.SetParent(null);
			_thisColl.enabled = true;
			_thisColl.isTrigger = false;

			if (_thisColl.attachedRigidbody == null) {
				gameObject.AddComponent<Rigidbody>();
			}
			yield return new WaitForSeconds(3f);
		}
		IEnumerator Drow() {
			while (transform.localPosition != Vector3.zero && transform.localScale != Vector3.zero) {
				transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, Time.deltaTime);
				transform.localScale = Vector3.MoveTowards(transform.localScale, Vector3.zero, Time.deltaTime);
				yield return null;
			}
		}
	}

	protected enum HideType {
		None, Peel, Drow
	}

#if UNITY_EDITOR
	[ExecuteInEditMode]
	private void OnValidate() {
		GetComponent<Collider>().isTrigger = true;
	}
#endif

}

