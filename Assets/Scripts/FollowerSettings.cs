﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CameraSettings", menuName = "RollerBall/CameraSettings", order = 1)]
public class FollowerSettings : ScriptableObject {

	[Range(1, 15)]
	public int FollowSmooth;
	[Range(25, 50)]
	public int RotationSmooth;
	public Vector3 LookOffset;
	public Vector3 LookEuler;

}
