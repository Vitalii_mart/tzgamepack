﻿using RayFire;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
/// Пример объекта-прилипалки
public class StickedPartial : StickedObject {

	[SerializeField] [Range(2,12)]
					 protected int _slices = 5;
	[SerializeField] protected MeshFilter _meshFilter;
	[SerializeField] protected MeshRenderer _renderer;

	private List<RayfireRigid> _slicedObj = new List<RayfireRigid>();

	protected override void Awake() {
		base.Awake();
		_meshFilter = GetComponent<MeshFilter>();
		_renderer = GetComponent<MeshRenderer>();
	}

	public override void OnGlued(IVelcro velcro) {

		base.OnGlued(velcro);
		_renderer.enabled = false;
		// Сломать меш и прикрепить его к шарику
		RayfireRigid rig = RayfireMan.inst.GetPoolObject(_meshFilter.sharedMesh, _renderer.material);
		rig.transForm.position = transform.position;
		rig.Initialize(_slices).Demolish(out _slicedObj);

		foreach (var f in _slicedObj) {
			RFPhysic.SetSimulationType(f, SimType.Kinematic, false);
			f.transform.parent = f.rootParent = transform;
			f.transform.localPosition = Vector3.zero;
			f.gameObject.layer = LayerMask.NameToLayer("Ragdoll");
		}
	}
	
	public override void OnPeelOff() {

		switch (_hideType) {
		case HideType.None:
			Destroy(gameObject);
			break;
		case HideType.Peel:
			CoroutineBehavior.StartCoroutine(PeelProcess(), () => Destroy(gameObject));
			break;
		case HideType.Drow:
			CoroutineBehavior.StartCoroutine(DrowLocalProcess(_slicedObj), () => Destroy(gameObject));
			break;
		}
		// Снимает меши по кусочкам
		IEnumerator PeelProcess() {
			while (_slicedObj.Count > 0) {
				yield return new WaitForSeconds(Random.Range(0.4f, 1.5f));
				var target = _slicedObj[_slicedObj.Count - 1];
				_slicedObj.RemoveAt(_slicedObj.Count - 1);

				RFPhysic.SetSimulationType(target, SimType.Dynamic, true);
				target.fading.onActivation = true;
				target.fading.fadeType = FadeType.MoveDown;
				target.Activate();

				target.transForm.SetParent(null);
			}
		}
		// Топить объекты в локальных координатах
		IEnumerator DrowLocalProcess(List<RayfireRigid> targets) {
			bool flag = true;
			while (flag) {
				flag = false;
				foreach (var f in targets) {
					if ((f.transForm.localScale = Vector3.MoveTowards(f.transForm.localScale, Vector3.zero, Time.deltaTime)) != Vector3.zero) {
						flag = true;
					}
					if ((f.transForm.localPosition = Vector3.MoveTowards(f.transForm.localPosition, Vector3.zero, Time.deltaTime)) != Vector3.zero) {
						flag = true;
					}
				}
				yield return null;
			}
		}
	}
}