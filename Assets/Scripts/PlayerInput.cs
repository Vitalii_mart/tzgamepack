﻿
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	private static PlayerInput _instance;

	private static PlayerInput Instance { get {
			if (_instance == null) {
				GameObject go = new GameObject("PlayerInput");
				#if UNITY_EDITOR
				_instance = go.AddComponent<KeyInput>();
				#elif UNITY_ANDROID
				_instance = go.AddComponent<TouchInput>();
				#endif
			}
			return _instance;
		}
	}

	public static float HorizontalAxis() {
		return Instance._horizontalAxis;
	}
	protected virtual float _horizontalAxis { get; set; }

}

public class TouchInput : PlayerInput {

	private void Awake() {
		_maxOffset = Screen.width / 4f;
	}

	private float _maxOffset;
	private Vector2 _firstTouch = Vector2.zero;

	private void Update() {

		if (Input.touchCount > 0) {
			var tch = Input.touches[0];
			switch (tch.phase) {
			case TouchPhase.Began:
				_firstTouch = tch.position;
				_horizontalAxis = 0f;
				break;
			case TouchPhase.Moved:
			case TouchPhase.Stationary:
				_horizontalAxis = Mathf.Clamp((tch.position - _firstTouch).x / _maxOffset,-1f,1f);
				break;
			case TouchPhase.Ended:
			case TouchPhase.Canceled:
				_firstTouch = Vector2.zero;
				_horizontalAxis = 0f;
				break;
			}
		}
	}
}
public class KeyInput : PlayerInput {

	protected override float _horizontalAxis => Input.GetAxis("Horizontal");

}
