﻿

using UnityEngine;


/// Расширения
public static class StaticExtensions {

	public static Vector3 Direction(this Vector3 from, Vector3 to) {
		return (to - from).normalized;
	}

}
