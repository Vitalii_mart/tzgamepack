﻿
using System.Collections;
using UnityEngine;
using System;

/// Более управляемые курутины
public class CoroutineBehavior : MonoBehaviour {

	private static CoroutineBehavior _instance;

	public static CoroutineWrapper StartCoroutine(IEnumerator coroutine, Action endCallabck = null) {
		if (_instance == null) {
			GameObject go = new GameObject("CoroutineCenter");
			_instance = go.AddComponent<CoroutineBehavior>();
		}
		var cor = new CoroutineWrapper(coroutine, _instance, endCallabck);
		cor.Start();
		return cor;
	}

}

public class CoroutineWrapper {

	private CoroutineWrapper() { }
	public CoroutineWrapper(IEnumerator coroutine, MonoBehaviour mono, Action endCallback = null) {
		_mono = mono;
		_coroutine = coroutine;
		_endCallback = endCallback;
	}

	private Action _endCallback;
	private MonoBehaviour _mono;
	private IEnumerator _coroutine;

	public bool IsPaused { get; private set; }
	public bool IsRunning { get; private set; }

	public void Start() {
		if (IsRunning) {
			return;
		}
		IsRunning = true;
		_mono.StartCoroutine(Wrapper());
	}
	public void Pause() {
		IsPaused = true;
	}
	public void Resume() {
		IsPaused = false;
	}
	public void Stop() {
		IsRunning = false;
	}
	private void End() {
		_endCallback?.Invoke();
	}

	private IEnumerator Wrapper() {
		IEnumerator i = _coroutine;
		yield return null;
		while (IsRunning) {
			if (IsPaused) {
				yield return null;
			} else {
				if (i != null && i.MoveNext()) {
					yield return i.Current;
				} else {
					IsRunning = false;
				}
			}
		}
		End();
	}
}





