﻿
using UnityEngine;

public interface IVelcro {
	Collider VelcroColl { get; }
	Rigidbody VelcroRigi { get; }
	float VelcroSize { get; set; }
	void GlueMe(Vector3 point, IStickable obj);
}
public interface IStickable {
	void OnGlued(IVelcro velcro);
	void OnPeelOff();
	GameObject RootObject { get; }
	float Lifetime { get; }
}
public interface IDestroyable {
	// Толкает в основном шар своим размером
	void Push(float power);
	bool IsDestucted();
}
public interface IScorable {
	float GetBonusScore();
}