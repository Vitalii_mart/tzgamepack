﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// Компонент отвечающий за налипание объектов
public class Sticking {

	private IVelcro _velcro;
	private Transform _velcroTransform;
	private List<StickabObj> _gluedObjects = new List<StickabObj>();
	private CoroutineWrapper _timer;

	public void Setup(IVelcro velcro) {
		_velcro = velcro;
		_velcroTransform = velcro.VelcroRigi.transform;
		_timer = CoroutineBehavior.StartCoroutine(Timer());
	}

	public void GlueObject(Vector3 contact, IStickable obj) {
		obj.OnGlued(_velcro);
		if (obj is IScorable scor) {
			GameCenter.AddPlayerScore(scor.GetBonusScore());
		}
		obj.RootObject.transform.position = contact;
		obj.RootObject.transform.SetParent(_velcroTransform);
		_gluedObjects.Add(new StickabObj(obj));
	}

	private IEnumerator Timer() {
		while (true) {
			yield return new WaitForSeconds(0.5f);
			_gluedObjects.RemoveAll((x) => {
				if ((x.Lifetime += 0.5f) >= x.LifetimeLimit) {
					x.Obj.OnPeelOff();
					return true;
				} else {
					return false;
				}
			});
		}
	}
	private class StickabObj {
		public float Lifetime;
		public float LifetimeLimit;
		public IStickable Obj;

		public StickabObj(IStickable stickable) {
			Lifetime = 0;
			LifetimeLimit = stickable.Lifetime;
			Obj = stickable;
		}
	}
}

