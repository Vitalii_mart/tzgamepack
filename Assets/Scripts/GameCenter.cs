﻿

using RayFire;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class GameCenter : MonoBehaviour {


	public BallMovement Ball;

	public TextMeshProUGUI PlayerScoreLabel;

	private BallSettings _ballSettiings;
	private FollowerSettings _followerSettings;

	private static GameCenter _instance;
	private bool _isStarted = false;

	private float _score = 0;
	private float _playerScore {
		get { return _score; }
		set {
			_score = value;
			PlayerScoreLabel.text = _score.ToString("0");
			Ball.VelcroSize = _ballSettiings.DefaultSize + _score * _ballSettiings.ScoreToSizeMultiplier;
		}
	}

	private void Awake() {
		_instance = this;
	}
	
	private void OnGUI() {

		if (!_isStarted && GUI.Button(new Rect(0, Screen.height/2-50, Screen.width, 100), "Start")) {
			StarGame();
			_isStarted = true;
		}
		if (_isStarted && GUI.Button(new Rect(0, 0, Screen.width / 4, 80), "Reload")) {
			ReloadLevel();
		}

	}
	void StarGame() {
		RayfireMan.RayFireManInit();
		_ballSettiings = Resources.Load("DefaultSettings/BallSettings") as BallSettings;
		_followerSettings = Resources.Load("DefaultSettings/CameraSettings") as FollowerSettings;
		Ball.Setup(_ballSettiings, _followerSettings);
	}
	void ReloadLevel() {
		SceneManager.LoadScene(0);
	}


	public static void AddPlayerScore(float value) {
		_instance._playerScore += value;
	}





}
