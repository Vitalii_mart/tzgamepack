﻿
using System.Collections.Generic;
using UnityEngine;
using System;

// Тут это - камера
public class Follower {

	private FollowerSettings _settings;
	private Transform _followTarget;
	private Transform _followerTransform;
	private Func<Vector3> _getTargetDirectaion;
	private Func<float> _timeRate;

	public float DistanceMultiplier = 1f;

	/// <summary>
	/// 
	/// </summary>
	/// <param name="settings"> Настройки камеры</param>
	/// <param name="follower"> Трансформ объекта, который будем двигать</param>
	/// <param name="target"> За кем следим</param>
	/// <param name="timeRate"> TimeStump</param>
	/// <param name="directionIn"> Куда смотрит цель слежки?</param>
	public void Setup(FollowerSettings settings, Transform follower, Transform target, Func<float> timeRate, Func<Vector3> directionIn = null) {

		_settings = settings;
		_followTarget = target;
		_followerTransform = follower;
		_timeRate = timeRate ?? (() => { return Time.deltaTime; });
		_getTargetDirectaion = directionIn ?? (() => { return target.forward; });

	}
	/// Call like Update()
	public void Move() {
		if (!_followTarget) {
			throw new System.ArgumentNullException("First call Setup();");
		}
		CatchNextPoint();
		ApplyActualPoint();
	}

	#region Smooth points
	private Queue<Vector3> _followPoints = new Queue<Vector3>();
	private Vector3 _sumOfPoints;
	private Quaternion _targetRotation;

	private void CatchNextPoint() {

		_targetRotation = Quaternion.LookRotation(_getTargetDirectaion.Invoke()) * Quaternion.Euler(_settings.LookEuler);
		Vector3 newPoint = _followTarget.position + _targetRotation * (_settings.LookOffset * DistanceMultiplier);

		_sumOfPoints += newPoint;
		_followPoints.Enqueue(newPoint);

		if (_followPoints.Count >= _settings.FollowSmooth) {
			_sumOfPoints -= _followPoints.Dequeue();
		}

	}
	private void ApplyActualPoint() {

		_followerTransform.position = _sumOfPoints / _followPoints.Count;
		_followerTransform.rotation = Quaternion.LerpUnclamped(_followerTransform.rotation, _targetRotation, _settings.RotationSmooth * _timeRate.Invoke());

	}
	#endregion

	
}
