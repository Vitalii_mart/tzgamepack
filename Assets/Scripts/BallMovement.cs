﻿
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class BallMovement : MonoBehaviour, IVelcro {

	private BallSettings _settingsBall;
	private FollowerSettings _settingsCamera;

	private Transform _thisTransform;
	private Vector3 _movementDir = Vector3.forward;

	private Follower _camera = new Follower();
	private Sticking _sticking = new Sticking();

	#region IVelcro
	public Collider VelcroColl { get; private set; }
	public Rigidbody VelcroRigi { get; private set; }
	public void GlueMe(Vector3 point, IStickable obj) => _sticking.GlueObject(point, obj);

	public float VelcroSize {
		get { return VelcroRigi.mass; }
		set {
			_thisTransform.localScale = Vector3.one * value;
			VelcroRigi.mass = value;
			_camera.DistanceMultiplier = value / _settingsBall.DefaultSize;
		}
	}
	#endregion

	private bool _isInitialized = false;

	public void Setup(BallSettings ballSettings, FollowerSettings cameraSettings) {

		_thisTransform = transform;
		_settingsBall = ballSettings;
		_settingsCamera = cameraSettings;
		
		VelcroColl = GetComponent<Collider>();
		VelcroRigi = GetComponent<Rigidbody>();

		VelcroSize = _settingsBall.DefaultSize;

		_sticking.Setup(this);
		_camera.Setup(_settingsCamera, Camera.main.transform, _thisTransform, () => { return Time.fixedDeltaTime; }, () => { return _movementDir; });

		_isInitialized = true;

	}

	private void FixedUpdate() {

		if (!_isInitialized)
			return;

		var rotatePower = (_settingsBall.TurnSpeed * Time.fixedDeltaTime) * PlayerInput.HorizontalAxis();
		_movementDir = Quaternion.AngleAxis(rotatePower, Vector3.up) * _movementDir;

		VelcroRigi.AddForce((_movementDir * _settingsBall.Asseleration) * Time.fixedDeltaTime, ForceMode.VelocityChange);
		// Хочу убрать заносы и сделать шарик более управляемым + ограничить скорость
		var velocity = _movementDir.normalized * VelcroRigi.velocity.magnitude;
		velocity = Vector3.ClampMagnitude(velocity, _settingsBall.SpeedLimit);
		velocity.y = VelcroRigi.velocity.y;
		VelcroRigi.velocity = velocity;

		_camera.Move();

	}
	// Смотрим во что врезаемся
	private void OnCollisionEnter(Collision collision) {
		//collision.collider.GetComponent<IDestroyable>()?.Push(VelcroSize);
		if (collision.collider.CompareTag("Destroyable")) {
			var component = collision.collider.GetComponent<IDestroyable>();
			if (component != null) {
				component.Push(VelcroSize);
				if (component.IsDestucted() && component is IStickable s) {
					GlueMe(collision.contacts[0].point, s);
				}
			}
		}
	}
}

