﻿
using RayFire;
using UnityEngine;

/// Большой разрушаемый объект
[RequireComponent(typeof(RayfireRigid))]
public class DestroyableObject : MonoBehaviour, IDestroyable, IScorable {
	
	[SerializeField] private float _durability = 4f;
	[SerializeField] private float _bonusScoreAmount;
	[SerializeField] private RayfireRigid _rigid;
	private bool  _isDestucted = false;

	public bool IsDestucted() => _isDestucted;
	public float GetBonusScore() => _bonusScoreAmount;

	public void Push(float power) {
		if (_isDestucted || power < _durability)
			return;
		_isDestucted = true;

		_rigid.Initialize();
		_rigid.Demolish();

		GameCenter.AddPlayerScore(GetBonusScore());
	}
}
